---
layout: post
title: 'Mapping Brod - Joachim Bergerhoff'
author: "admin"
---
On February 10, 2010, FLOSSK held a meeting with Joachim Bergerhoff where he explained to us that UN-HABITAT had a project on mapping Brod, a small village in the Sharr Mountains that is facing serious environmental damage. FLOSSK helped mapping Brod on OpenStreetMap.
Watch the beautiful pictures during this mapping project of Brod, Kosovo nature: Click here
Here is the map from the project:


	View Larger Map
