---
layout: post
title: 'Free Software in Balkans 2010'
author: "admin"
---
On September 11-12 2010, FLOSS Albania and the University of Vlora, organized the first Conference for Software Freedom in the Balkans, South Albania.

	FreeSB 2010 (Free Software in Balkans) was the first conference of its kind for Free and Open Source Software in the Balkans. This conference was designed collection of professionals, academics and enthusiasts who share vision that the software should be Free and Open to the community to develop and adapt it to their needs.

	Consequently, FLOSS Kosovo decided to help create a broader conference in the Balkans.FRESSB aimed at making the conference a traditional event that regional audiences will gather once a year for lectures and presentations by professionals in various international and national levels who will present the latest developments in the general community and Free and Open Source Software. It will take place from one state to another state, every year in a row.In the conference that first was held in Albania, were invited lecturers from around the Balkans. Special focus was given to public administration, and also had plans for "Software Hacking Rooms" which aimed to various planning projects.

	For more information visit: http://freesb.eu
