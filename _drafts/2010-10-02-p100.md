---
layout: post
title: 'GIS Training Invitation from Joachim Bergerhoff'
author: "admin"
---
On 2nd October 2010, FLOSSK members were invited by Joachim Bergerhoff to be part of an introduction to Open Source GIS (Geographic Information System), with a training offered by Benjamin Ducke, consulant to the FSLU (Further Support to Land Use) Programme in Kosovo.

	Topics included:

		Installation of gvSIG

		Data integration,

		Data editing

	You can download the latest version of gvSIG here http://oadigital.net/software/gvsigoade/gvsigdownload

	The programme runs on Java, i.e. on all the usual operating systems. It edits shape files and many other formats. It includes the powerful GRASS (Geographic Resources Analysis Support System) and Sextante toolboxes, an integrated map editing facility and many other goodies.

	Here you can find a presentation given at the Software Freedom Kosova Conference 2010 from Benjamin Ducke.
