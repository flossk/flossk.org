---
layout: post
title: '“Mozilla Balkans” Meetup – Sofia, Bulgaria'
author: "altin"
---
![alt](/flossk.org/img/blog/Simage_thumb.png)
![alt](/flossk.org/img/blog/dscf0896.jpg)
![alt](/flossk.org/img/blog/5807935416_7ceb0f2103_b.jpg)
![alt](/flossk.org/img/blog/IMGP2888.jpg)
On 4th and 5th of June for the third time was held the Mozilla Balkans Community Meetup. This time the meeting was organized in  Sofia, Bulgaria.
	Even this time Kosova had its own representative. The representatives are part of Balkan communities, so each of them represent a country, and all are invited and  financially covered by Mozilla Europe so that they could attend this 2 day's event.

	The participant team at this meeting:

	

	The purpose of this meeting was to enable communities of Mozilla Ballkans to share and learn experiences from each other, thus improving cooperation for the future and work on certain issues based on the last meeting held in Ljublana of Slovenia, in which meeting Kosovo was represented by two members of its community too.

	Besides Kosovo, the event was attended by representatives from countries such as Bosnia, Bulgaria, Croatia, Greece, Macedonia, Romania, Serbia, Slovenia and Turkey.

	This third meeting was an opportunity to reflect agreements, challenges and opportunities of communities over a year, and to present the results of their work since the last meeting in Ljublana.

	As for the representatives from Kosovo, this time in Sofia, attended  Altin Ukshini.

	

	It's good noting that the meeting attendees had the honor to meet one of the CO-Founders and the first employee of Mozilla Foundation Chris Hoffman

	

	If you want to know more about this meeting, Click here to see the organization, schedule of the meeting etc.
