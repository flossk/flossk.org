---
layout: post
title: 'Kumevotu - Polling Station Project'
author: "admin"
---
![alt](/flossk.org/img/blog/ku_me_votu_logo.png)
FLOSSK with the financial support of UNICEF Innovations Lab on 12.December.2010 finished the Polling Station project for the Parliamentary Elections of 2010 in Kosovo.

	The project aimed making it as easy as possible for people, especially for the younger generation, to find their dedicated polling station, through the created website http://kumevotu.info

	This website has an interactive map based on “OpenStreetMap”  which shows the location of the X persons voting station.
	All you have to do is fill the fields with your personal information like name, surname, date of birth and the municipality and right after that, automatically in the right side appears the interactive map that shows you the location of your polling station.
	The need for such a service was particularly evident, as we observed that in large cities many people do not have their polling station on the one that its nearest to them, but in an arbitrarily assigned one that was chosen by government and in this case many young people didn’t knew where their new polling station is.

	During this project there were achieved these following steps:

		It took place a course that trained students from three cities of Kosovo on OSM and more generally GIS,

		Collection, editing and entry of  (POI) Points of Interest of approximately all schools in main cities in OSM,

		Convert-ion of the official lists of registered voters from PDF to CVS,    

		Creation of kumevotu.info website which became functional in time for the elections and which was clicked/used 1800 times, you can have a look at the statistics: http://ur1.ca/2qq1n.      

		Plan of releasing the code of the web site under GPL license on     http://github.com/flossk.

	All this work was done for less than two weeks of hard work by a group of young FLOSSK members.

	View the FLOSSK Gallery to see pictures from the course that was held during this project: Click here
