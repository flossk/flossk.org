---
layout: post
title: 'Wikipedia and Wikimedia'
author: "admin"
---
![alt](/flossk.org/img/blog/wikipedia.png)
Wikimedia and Wikipedia are also one of the main projects of FLOSS Kosova, through which we try to promote and distribute open education in Kosovo
	FLOSSK members also regularly continue editing Wikipedia.
	Some of them do it on their free time and others take it as hobby, usually the write new articles but most commonly they do a lot of translation of various materials in Albanian.

	Albanian-language version of this free encyclopedia exists under the address: http://sq.wikipedia.org and today contains more than 31,458 items, and is listed on the 55-th place in the range Meta.
	Not to Forget, I’d like to mention that among the biggest editors of materials that have Albanian meaning and also Albanian written content, is our Organizations Co-Founder itself James Michael DuPont, who has written more than 3000 articles till now, here is link:http://en.wikipedia.org/wiki/User:Mduponthttp://en.wikipedia.org/wiki/Special:Contributions/Mdupont

	What is Wikimedia, Wikipedia and Encyclopedia ?

	Wikipedia is a multilingual, web-based, free-content encyclopedia project based on an openly editable model. Wikipedia's articles provide links to guide the user to related pages with additional information.
	Wikipedia is written collaboratively by largely anonymous Internet volunteers who write without pay. Anyone with Internet access can write and make changes to Wikipedia articles (except in certain cases where editing is restricted to prevent disruption or vandalism). Users can contribute anonymously, under a pseudonym, or with their real identity, if they choose.

	Most of Wikipedia's text and many of its images are dual-licensed under the Creative Commons Attribution-Sharealike 3.0 Unported License (CC-BY-SA) and the GNU Free Documentation License (GFDL) (unversioned, with no invariant sections, front-cover texts, or back-cover texts). Read More

	Encyclopedia is a work of great scientific knowledge that includes all areas of life and activity, systematized in a summary in the form of a dictionary.
	Such works that include knowledge of a separate branch of science, art, technology, etc..
	is a type of reference work, a compendium holding a summary of information from either all branches of knowledge or a particular branch of knowledge.[1]
	Encyclopedias are divided into articles or entries, which are usually accessed alphabetically by article name. Encyclopedia entries are longer and more detailed than those in most dictionaries. Generally speaking, unlike dictionary entries, which focus on linguistic information about words, encyclopedia articles focus on factual information to cover the thing or concept for which the article name stands.

	Wikimedia Foundation, is an American non-profit organization that has the headquarters in San Francisco, California, United States, and organized under the laws of the State of Florida, where its originally based.
	It operates several online collaboration wiki projects including Wikipedia, Wiktionary, Wikiquote, Wikibooks, Wikisource, Wikimedia Commons, Wikispecies, Wikinews, Wikiversity, Wikimedia Incubator and Meta-Wiki.

	For more information visit:http://wikipedia.orghttp://en.wikipedia.org/wiki/Main_Page
