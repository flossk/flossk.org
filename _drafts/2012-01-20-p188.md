---
layout: post
title: 'FLOSSTalk with Arturo Suarez: OpenStack'
author: "gent"
---
![alt](/flossk.org/img/blog/openstack-logo512_125x125.png)
On the 22nd of February starting from 18:00 Arturo Suarez from Stackops.com will be doing a discussion on our next FLOSSTalk about OpenStack, the free software cloud computing project. The presenation will happen at UNICEF Innovations Lab Kosovo in Prishtina which is located in Str. "Gazmend Zajmi", no 59 (check the map below).
The talk will go on for 45 min  + 15 min Q&A. Arturo is going to join us via video conference from Madrid. We will be live streaming the event too, just like the last time for those who can't join us.
Please be sure to register on our Facebook event.
UNICEF Innovations Lab Kosovo on the map:

View Larger Map
