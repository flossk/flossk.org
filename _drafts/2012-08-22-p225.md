---
layout: post
title: 'FLOSSK Teambuilding event Gjilan Hotel Kristal'
author: "mdupont"
---
We will be meeting with all flossk members and interested people in Gjilan. Topic : SFK12, Flossk in Gjilan, Openstreetmap, Wikipedia When : Postponed for after the SFK12 Where : Hotel Kristal, 42.46357,21.470161 FOSM : http://map.fosm.org/?zoom=18&lon=21.46982&lat=42.46358&layers=B0F Openstreetmap :http://www.openstreetmap.org/browse/node/423846912 http://www.openstreetmap.org/?lat=21.470161&lon=42.46357&zoom=18&layers=M# Contact People : * Dardan : http://www.flossk.org/en/users/aliudardan * Besfort : http://www.flossk.org/en/users/besiguri External Links : Google Plus : https://plus.google.com/101434835117282349740/about?hl=en 4square : https://de.foursquare.com/v/hotel-kristal/4db1797493a0615768553ce3 Facebook: https://www.facebook.com/events/372914276114700
