---
layout: post
title: 'Software Freedom Kosova 2011 is coming'
author: "altin"
---
![alt](/flossk.org/img/blog/sfk11-final-small.jpeg)
Software Freedom Kosova Conference 2011 is the third annual conference about Free/Libre and Open Source Software (FLOSS) to be held in Prishtina on November 12th.
The event is open but we recommend you to reserve your seat: http://sfk11.eventbrite.com/
The Conference gathers professionals, academicians and enthusiasts who share the vision that software should be free and open for the community to develop and customize to its needs, including commercial ones, and that knowledge is a communal property and free and open to everyone.
FLOSS Kosova has succeeded in making the conference a traditional event, which will gather a regional audience yearly for lectures and presentations by a variety of international and national professionals and academicians who will present the latest developments in the global FLOSS community.
And just to mention that the first conference took place in August 2009 with some 500 attendees and speakers from Sun Microsystems, Mozilla, MIT, European Union Government Open Source consultants and others and the second conference took place in September 2010 and had several FLOSS luminaries present.
Theme of SFK11
The theme of the 2011 conference will be “Doing Business with Free Software”. The conference will be used as a meeting point for a network of individuals and companies with the aim of sponsoring and initiating projects which benefit the people of Kosovo by establishing it as one of the European centers of FLOSS, and ultimately result in commercial benefits for the country.
	The conference aims to introduce Kosovo businesses and public to a low cost alternatives rather than expensive proprietary software, train local people in the latest developments in software and social-collaboration tools, promote respect for legal licenses, and introduce the public to the best methods for localization of software and knowledge.
The conference will present speakers from the business world that use FLOSS in their commercial development projects. It will also present examples of companies from the region that are using FLOSS as a competitive leg up in the outsourcing European market. Free software is especially suitable to this type of economic activity as it presents very low barriers to entry by being free at acquisition, well documented and having a free ecosystem of support. Moreover, by presenting free code to build on, it helps entry level professionals and companies build up skills and work up the leader towards more value added projects.
To speakers and people from the region attending this conference, please visit http://www.flossk.org/en/sfk11-pois to view a map with Points of Interest (Restaurants, Hotels, Caffes, Banks etc) in Prishtina.
	Below you can download the Conference Schedule/Programme.
