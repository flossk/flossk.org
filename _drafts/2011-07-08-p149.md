---
layout: post
title: 'Fedora 15 Release Party'
author: "altin"
---
![alt](/flossk.org/img/blog/Fedora_kosova.jpg)
Today as you could have been noticed before from our blog and also PC World Albania, in Prishtina was held the Fedora 15 Release party, to celebrate the release of the newest version of Fedora operating system.

		The Event was held in UNICEF Innovations Lab's office in Prishtina.



		The meeting/party didn't start as it was expected, same happened with the prepared presentations that were supposed to start at 06:00 PM, because of the low number of participants, even that there were a lot of words from people coming.

		With the presentations started there were shown the new features of Fedora 15 including discussions with the participants.
		The presenter was Gent Thaçi, who with some main points described shortly the most interesting things and their characteristics of this version.
		The Participants as always very welcomed, had chance to take some symbolic presents such as Fedora stickers and even CD's with the latest version of Fedora on different desktop environments like GNOME 3, KDE and XFCE
