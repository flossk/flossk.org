---
layout: post
title: 'RememberKosovo GreaseMonkey script'
author: "agron"
---
There are some websites that don't let your browser remember things that you have typed.
Even though your browser has the capability to remember the entered data, on certain websites your browser doesn't even ask.
This simple script will turn back on the remembering feature which will ask you if you want the entries remembered.
So if you want to try it out, you can get it from http://userscripts.org/scripts/show/109413
The script works on Firefox, but you have to install the GreaseMonkey addon first, and on Chrome, without the need to install GreaseMonkey.
