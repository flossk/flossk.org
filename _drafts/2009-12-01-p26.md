---
layout: post
title: 'Presentation by MySoeciety.org'
author: "admin"
---
![alt](/flossk.org/img/blog/ms-tb.jpg)
MySociety.org is an e-democracy organization and a UK-registered charity named UK Citizens Online Democracy, that aims to build "socially focused tools with offline impacts".

	Tony Bowden's project was to find and support the development of transparency and democracy websites in central and eastern Europe. He was on tour in the Balkans looking for interested groups of people to support. He passed through Prishtina on December 9th.

	This was relevant to FLOSSK for two main reasons:

		Funded projects will be required to follow Free/Open Source licensing and development practices and adhere to appropriate Open Data principles, and

		FLOSSK can be among the innovators who apply the web for positive social ends in Kosovo.

	During his speech Tony Bowden talked about the role of FLOSS in transparent government and explained the projects of MySociety.org. We also showed him our members and FLOSS projects.
	Here you can find some pictures from the speech.
