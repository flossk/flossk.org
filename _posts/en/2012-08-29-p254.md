---
layout: post
lang: en
category: en
title: 'FLOSSK meeting in Skopje(Kika Hacklab)'
author: "BleonaFoniqi"
---
We are going to Kika hacklab in Skopje.KIKA means: Kika is Information, Kulture, Autonomy.In KIKA you can meet the local Python, Ruby, GNU/Linux and Free Software communities, and join their workshops, lectures, hacking and breaking stuff.

We are leaving at 07:25 am from the bus station in Prishtina.

The ticket price is 5,50 €

if you need financial help for the trip and you are a FLOSSK active contributor,then write an email to [email protected] and we can see what we can do.

Thanks.

Facebook event:
https://www.facebook.com/events/354363534638891/

Google Plus:
https://plus.google.com/u/0/events/c18dlcse1jb8tmdq3aergoegs88/106785192512941136314
