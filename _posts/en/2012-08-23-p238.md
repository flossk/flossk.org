---
layout: post
lang: en
category: en
title: 'Day Two FLOSSK Prishtina meeting at Innovations Lab 11:00 - 13:00 Aug 24'
author: "mdupont"
---
![alt](/flossk.org/img/blog/FZF10.png)

We are gathering for a meeting to build the team to help us for the conference, our idea is to meet with new people that are interested to help us for the conference and start experiencing the real FLOSSK work

We will be meeting at the Innovations Lab Kosovo, and start discussing about some ideas for the conference also present and meet with new members.

If you are interested and have time please don't hesitate to come everyone is invited, it will be fun.

Here is the place where you can find us

Innovation Labs Kosovo
Gazmend Zajmi No. 59, 10000
Prishtina, Kosovo http://www.openstreetmap.org/?lat=42.65582&lon=21.16665&zoom=16&layers=M...http://kosovoinnovations.org/contact

if you have any trouble to find the lab or anything please email me [email protected]

External Links :
* facebook event : http://www.facebook.com/events/444033918975217/
* SFK12 : http://www.flossk.org/en/conference/sfk12-software-freedom-kosova-2012
