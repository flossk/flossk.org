---
layout: post
lang: en
category: en
title: 'FLOSSTalk with Arangel Angov: Hacker Spaces'
author: "altin"
---
![alt](/flossk.org/img/blog/kika-hacklab.jpeg)
The next FLOSSTalk will be held on Saturday, 17th of March starting at 13:00 in the premises of Unicef Innovations Lab. The talk will be about hacker spaces also known as hacklabs - free technological areas where people interested in hacking and sharing experience and information come together to learn from each other and build interesting stuff.

Arangel Angov is one of the founding members of Free Software Macedonia and the hacker space KIKA in Skopje. He's been involved in the FOSS community for the past ten years. He's also been working as a GNU/Linux systems administrator for 8 years now.

"I'll try to give an in depth view of what is going on in the Skopje Hacklab KIKA, how it was started, how we manage to maintain it and what exactly is necessary to get up and running a hacker space in your city," says Arangel.

You can confirm your attendance in the created event in Facebook and in the map below you can find the exact location of Unicef Innovations Lab office.

For those who dont understand the map, here is a video.

View Larger Map
