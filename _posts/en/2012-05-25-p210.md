---
layout: post
lang: en
category: en
title: 'Tech Talk me Besfort Gurin'
author: "altin"
---
![alt](/flossk.org/img/blog/tech_talk3_sq.jpg)
Innovations Lab dhe FLOSSK ju ftojnë në "TechTalk"-un e tretë që do të mbahet të martën me 29 Maj në ora 18.30 në hapësirat e Innovations Lab Kosovo.

Ligjëruesi i radhës do të jetë Besfort Guri, anëtar i bordit të FLOSSK, i cili do të flasë për QGIS si Sistem Informativ Gjeografik me kod të hapur.

Në pjesën e parë, temat që do të shtjellohen janë:
- Si të merren dhe shtohen të dhënat në QGIS;
	- Si të shqyrtohen atributet e të dhënave tuaja gjeografike;
	- Si të bashkohen të dhënat private me ato publike;
	- Si të lidhen fotografitë më të dhënat tuaja gjeografike;
	- Si të ndryshohet dizajni i hartës tuaj;
	- Si ta printoni hartën tuaj;

Në pjesën e dytë, Besforti do të ligjërojë për GeoServer: cka është ai, formatet e fajllave që mund të shfrytëzohen me GeoServer, publikimi i tyre ne web dhe do ta demonostrojë këtë me dy shembuj:
- Do të ndërtohet me anë të GeoServer një hartë me orthophoto mbi të cilën do të vendosen të dhënat e vektorizuara nga OpenStreetMap.
	- Si të përdoret GeoServer që të krijohet një Web Map Service (WMS) server për editimin e OpenStreetMap me anë të orthophoto-ve nga GeoServer.

Besforti ka kontribuar në organizimin e konferencës Software Freedom Kosova, projektet e Prishtina Buses dhe Ku me Votu si dhe manualit të parë për krijimin e hartave me Open Street Map. Ai gjithashtu është anëtar i Humanitarian OpenStreetMap Team.

Mireupafshim,

Innovations Team & Flossk Team

	-------------------
Linqe:
Eventi në Facebook: https://www.facebook.com/events/355837551136111/
QGIS - www.qgis.org
GeoServer - www.geoserver.org
Open Street Map handbook: http://www.flossk.org/sq/projektet/doracaku
Innovations Lab Kosovo lokacioni:
	Video: http://www.youtube.com/watch?v=xjDpelIOupQ
	Harta:

View Larger Map
