---
layout: post
lang: en
category: en
title: 'Geolocation for stationary computers'
author: "agron"
---
First of all, geolocation services for stationary computers like desktops and kiosks are not in high demand. People using these devices are definitely not in motion and they don't need to be guided by a navigation system. 

But there are other reasons to use geolocation services that will save you a lot of zip code entering. Finding the nearest store, nearest red-box kiosk, measuring the distance between your home and the new doctors office, or figuring how long it takes to travel to some place from your current location.

Other things you need this feature for is Google Latitude to see where your kids and friends are and to also let them know, only if you wish to, where you are.

Some people want to check the gas prices around them before they go to work. They can use Gasbuddy.com without entering the zip code. Or check the weather, the traffic and so on.

Your privacy settings about your  location are always preserved. You will be prompted for every site that uses geolocation services to allow it or not, if did choose to be prompted.

Back in September 2011 Google changed the data structure of it's geolocation services and Firefox 9 followed right after that. Bellow you will find this new structure:

On your Linux based system, create a file called mylocation.json in your home folder. For me it would be /home/agron/

Paste the following content in mylocation.json
```
{"status": "OK",
"accuracy": 1,
"location": {
            "lat": 42.66448, 
            "lng": 21.16336}}
 
```
Just instead of 42.66448 and 21.16336 enter your own location. If you leave it this way, your friends in Google Latitude and Facebook Places will think you are in a Government building in Prishtina, Kosovo.

After this go into Firefox 9 and type about:config in the address bar and hit Enter. You need to promise that you know what you're doing.

You will have another search bar in that page. In case you did this before, type geo in that search bar just to see if there is an entry such as geo.wifi.uri . If there isn't, right click on the white space and select New > String . and type geo.wifi.uri, hit Enter and then enter something like file:///home/agron/mylocation.json. But instead of agron type in your name of whatever your home directory is.

If you already have   geo.wifi.uri .just double click on it  to update the value to your version of  file:///home/agron/mylocation.json. Please note that after file: there are 3 forward slashes.

After this just close the about:config tab and you ready to go.

One other benefit of this feature is that your wireless network is not scanned anymore. Many have seen it as a privacy  problem with sending over to Google the list of wifi mac addresses of their neighborhood. Not because of your privacy concerns, but because of your neighbor who might not want to share his mac address and location with google.
