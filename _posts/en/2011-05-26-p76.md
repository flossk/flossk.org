---
layout: post
lang: en
category: en
title: 'Drupal project in FLOSSK'
author: "admin"
---
![alt](/flossk.org/img/blog/drupal-logo.jpg)

Despite that FLOSSK continues promoting this Open Source CMS (Content Management System), among the main issues is that FLOSSK is already using this CMS.

Drupal is an open source Content Management Platform (CMS) written in PHP, under Open GPL license used by millions of websites and applications in the world.

The CMS is built, used and supported by many communities and active people in technology, especially supporters of Free and Open Source Software.

Among the most popular sites that are built on Drupal is the U.S. Government (White House) site http://www.whitehouse.gov/ you can prove it here http://isthissitebuiltwithdrupal.com/

Why to use Drupal?

Use Drupal to build every kind of personal blog or website or even more advanced enterprises and large firms websites. Hundreds of add-ons modules and designs allow you to build multiple website that you have imagined.

Drupal is free, flexible, and constantly modified by hundreds of thousands of passionants from around the world, and hundreds of thousands of people and organizations use Drupal.

Join Drupal!

For more visit the homepage of Drupal:http://drupal.org