---
layout: post
lang: en
category: en
title: 'Linux Course'
author: "admin"
---
Around the world there are countless training providers for GNU / Linux,there should be noted that this was pretty much supported also by FLOSSK, which has contributed on this with its capacities and its knowledge of Free and Open Source Software.

In February 2009 the Co-Founder of FLOSSK James Michael DuPont who began holding a course for the GNU / Linux operating system here in Pristina in offices of IDI (Information Development Initiative), course which was free only to encourage youngsters in Kosovo to use and learn about Free Software.

Back then there were quite interested people in this course, but after that J.M.D had to leave Kosovo and that course was paused, but it continued in June of that year by some of his own students who continued to work on giving lectures for GNU / Linux by following their teachers steps.

We wish that also in the future to have the financial capacity along with the volunteering so that we can continue these types of courses as we did before...
