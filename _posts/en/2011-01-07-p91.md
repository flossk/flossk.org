---
layout: post
lang: en
category: en
title: 'Drupal 7 Release Party'
author: "admin"
---
![alt](/flossk.org/img/blog/d7.jpg)
On January 7, 2011,  FLOSSK held an event in its office on the occasion of the new release of the free Content Management System Drupal 7. A presentation on Drupal 7 was done by Gent Thaci, a member of FLOSSK.

Installation and testing of Drupal 7 in one of the hacklab computers of FLOSSK was done by Altin Ukshini. Drupal and Open Atrium stickers were also handed out.

Check out the pictures and the official Drupal event page.http://groups.drupal.org/node/114709http://gallery.flossk.org/index.php?level=album&id=22
