---
layout: post
lang: en
category: en
title: 'How you can do something for flossk ?'
author: "mdupont"
---
Here is a short list of things you can do to help flossk:

* Join the flossk.org website and blog about flossk topics
* Help translate flossk.org to Albanian
* Promote flossk events and projects on social media, create videos and social buzz about flossk topics
* Find sponsors for flossk, in terms of hardware, money, food and water
* Help organize flossk events in your town, you can invite the flossk team to come to your town
* Join flossk related projects for example wikipedia, mozilla, openstreetmap, fedora, fsfe.org, many other free/libre open source software projects etc. You can help take pictures of kosovo
* Talk to educators, businessmen, politicians, computer people and your friends about flossk and flossk projects
* Buy flossk t-shirts and stickers to support flossk
* Join our mailing lists, follow us on indenti.ca join our groups on facebook, follow us on twitter, join our group on linked in
