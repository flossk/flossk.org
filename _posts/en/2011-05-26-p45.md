---
layout: post
lang: en
category: en
title: 'Localization - Translations'
author: "admin"
---
![alt](/flossk.org/img/blog/pic_ie10.jpg)
Localization of software has been an objective of our organization since the beginning, thinking that free software can not be promoted without first being translated and localized in Albanian language.

FLOSSK members regularly translate on their free time.

Among the programs, applications, and sites where we work/worked are:

- Mozilla Firefox
- Ubuntu, distribution of GNU / Linux, translated up to 50%, click here
- OpenOffice http://sq.openoffice.org/
- Sugar Labs http://translate.sugarlabs.org/sq/
- Chromium Web browser
- Web site of Ushaidi ushaidi.com
- Wikipedia, Wikimedia

Be a part of this initiative and help us translate Free Software in Albanian, help our community and ensure a future with a better education in Albanian.

If you’d like to contact us for anything that has to do with translations, mail us on [email protected]
