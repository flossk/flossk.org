---
layout: post
lang: en
category: en
title: 'A Hello world in Arduino'
author: "altin"
---
![alt](/flossk.org/img/blog/arduino_hello_world.jpg)
This is the most basic programming that you could do with an arduino uno.

It just endlessly turns the light built in the arduino on and off.

So, after having an arduino uno and a usb cable to connect it to your pc, you would want to have also the arduino software.

* Get the software here: http://arduino.cc/en/Main/Software
* Extract it into a folder
* Plug in your arduino in pc
* Run the program
* Copy paste the code below on the arduino software
* Save it and upload it into your arduino... and after the arduino resets you will have your light blinking...

Code:
```
/*
Hello World program in arduino
*/
 
int Pin = 13;
 
void setup()
{
  //initialize pins as outputs
  // Here we are seting pin 13's mode to output
  pinMode(Pin, OUTPUT);
}
 
// The loop where this code will be executed endlessly
void loop()
{
  digitalWrite(Pin, HIGH); // Turn on
  delay(1000); // delay 1000mili seconds (1sec)
  digitalWrite(Pin, LOW); // Turn off
  delay(1000); //delay again
}
```