---
layout: post
lang: en
category: en
title: 'Mozilla Day in Gjakova'
author: "admin"
---
For the first time in Kosovo, on 16 October 2010 FLOSSK organized a Mozilla Day in the town of Gjakova, in the library "Ibrahim Rugova". Discussions centered around Mozilla and its projects such as:

* Firefox and Thunderbird,
* SpreadFirefox,
* Mozilla Camp Reps,
* progress with Firefox 4.0
* localization,
*  translation into Albanian
*  Thunderbird, and
Firefox Add-ons .

Speakers at the event were

* Heroid Shehu,
* Jon Berisha, and
* Endrit Vula.

For more about Mozilla visit:http://www.mozilla.org
