---
layout: post
title: 'Software Freedom Day në Liceun Elektro Teknik "Don Bosko"'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/sfd-donbosko.jpg)
![alt](/flossk.org/img/blog/regular_smile.gif)
Software Freedom Day (SFD) është një festë vjetore në mbarë botën e Free Software.

SFD është një përpjekje e arsimit publik me qëllim të ngritjes së vetëdijes së Free Software dhe virtytet e tij, dhe gjithashtu inkurajimin e përdorimit të saj.

Mu në ditën kur u paraqit në treg sistemi i shumëpritur Windows 7, FLOSSK Organizoi Ditën e Softuerit të lirë në Liceun Elektroteknik “Don Bosco” në Prishtinë.

Konferenca u organizua nga FLOSS Kosova në kooperim me Liceun Elektroteknik “Don Bosko” ku gjatë këtij prezantimi komuniteti i Liceut, duke përfshirë studentët dhe profesorët, u njoftuan për së afërmi me kodin e hapur dhe qëllimin e tij, GNU/Linux-in, Open Street Map, Wikipedia, e gjëra të tjera në lidhje me softuerin me kod të hapur.

Ne prezentim moren pjese:

- Taulant Ramabaja, i cili prezantoi Wikimedian dhe Wikipedian
- Gent Thaqi, projektin OLPC
- Ardian Haxha, informoi studentet reth GNU/Linux
- Valdrin Maliqi, prezantoi Organizaten FLOSSK dhe projektin OpenStreetMap

Siq thotë një fjalë e urtë “Nje gurë dy Zogj” kështu ndodhi ne këtë konferencë ku FLOSSK bëri prezëntimin e lëshimit te versionit të ri të Ubuntu 9.10 me qrast Studentëve dhe profesorëve ju shperndanë CD të ardhura direkt nga Ubuntu.

Për të Parë fotot klikoni ketu: Kliko Ketu
Nuk ka shumë foto megjithatë, mjaftueshëm sa për provë