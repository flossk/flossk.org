---
layout: post
title: 'Projekti i Fedora-s në FLOSSK'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/fedora-logo.png)
Projekti Fedora është një partneritet i anëtarëve të komunitetit për softuer të lirë dhe të hapur nga e gjithë bota. Projekti Fedora ndërton komunitete të softuerit të lirë dhe të hapur dhe prodhon një sistem operativ të bazuar në GNU/Linux të quajtur Fedora. 

FLOSSK në çdo lansim të verzioneve të reja të Fedora-s me ndihmën e komunitetit të FedoraProject organizon ngjarje dhe bën shperndarjen e CD-ve të këtij sistemi operativ

FedoraProject ka poashtu edhe dy Ambasadorë zyrtar në Kosovë –Ardian Haxha dhe Gent Thaci – të cilet janë poashtu edhe anëtarë të FLOSSK-ut.

Më shumë rreth projektit të Fedora-s:https://fedoraproject.org/https://fedoraproject.org/wiki/http://en.wikipedia.org/wiki/Fedora_%28operating_system%29https://fedoraproject.org/wiki/Releases/13/FeatureList
