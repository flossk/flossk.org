---
layout: post
title: 'Projekti OLPC'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/olpc-square_logo.jpg)
Projekti OLPC ka qenë prezent qysh në fillet e kësaj organizate, kur anëtarët tanë kanë dhënë kontributin e tyre më të madh për promovimin e këtij projekti edukativ duke filluar nga prezantimet e ndryshme nëpër institucione shkollore dhe gjatë Konferencës vjetore që FLOSSK ka organizuar.

	One Laptop Per Child (OLPC; shqip: Një Laptop Për Fëmijë) ka synim të fuqizojë fëmijët më të varfër të botës nëpërmjet arsimit duke siguruar për çdo fëmijë laptop me qmim të ulët dhe me shpenzim të ulët të energjisë.

	Për këtë qëllim, Projekti OLPC ka projektuar harduer dhe softuer aplikativ të përdorshëm nga fëmijët për të mësuar së bashku. Me qasje në këtë lloj mjeti, fëmijët angazhohen në vet-edukim: mësojnë, bashkpunojnë dhe krijojnë së bashku. Duke u lidhur me njëri-tjetrin ata lidhen me botën për një të ardhme më të ndritshme.

	Laptopi OLPC ka sistem operativ të bazuar në Fedora Linux, ka interfejsin Sugar dhe aplikacione të ndryshme të përshtatshme për mësim nga moshat e ulëta.
	Laptopët kanë internet pa tela që lejonë lidhjen me pika qasje standarde ose në rrjete 'mesh', që mundëson lidhjen e secilit laptop me shokët dhe shoqet përafër për bashkpunim. Këtë lloj interfejsi mund ta shkarkoni nga interneti dhe ta provoni edhe në kompjuterin tuaj.

	Nëse doni të dini më shumë rreth këtij projekti vizitoni http://one.laptop.org/

	Principet dhe edukimi i fëmijëve:



	 

	Laptopi XO, dizajn për të mësuar:
