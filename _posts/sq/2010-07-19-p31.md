---
layout: post
title: 'FLOSS Kampi në Shengjin'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/floss-camp.jpg)
Më 17 – 18 Korrik në Shëngjin të Shqipërisë, përfundoi FLOSS CAMP I organizuar nga FLOSS Kosova dhe FLOSS Albania.

Të pranishëm në ngjarje ishin shumë anëtarë të FLOSS Kosova dhe adhurues të softuerit të lirë dhe të hapur të cilët janë duke dhënë një kontribut të madh në zhvillimin e softuerit të lirë dhe të hapur në Kosovë, e gjithashtu po kontribojnë maksimalisht në mbështetjen e komunitetit të lirë edhe në Shqipëri.

Ata kanë sigurar edhe wirless pa pagesë për të gjithë të pranishmit në event.

Edhe pse ishte paraparë një pjesëmarrje më e madhe, pavarësisht ky takimi u përcoll me aktivitete mjaft atraktive grupore dhe me diskutime të ndërlidhura me softuerin e lirë dhe të hapur. Dhe gjatë dy ditëve pati edhe vizita nga njerëz të ndryshëm nga Shqipëria, por të cilët ishin vetëm vizitor. Në ditën e dytë “FLOSS Camp” u vizitua edhe nga një grup fëmijësh të interesuar në softuerin e lirë dhe të hapur, të cilët i njoftoi më shumë James Michael DuPont.

James Michael DuPont, njëri prej themeluesve kryesor të FLOSS Kosova, në fjalimin e shkurtë hyrës në ditën e parë, shpjegoi arsyen e takimit dhe theksoi sërish rëndësinë e takimit që komuniteti shqiptar të afrohet më shumë mes vete në këtë fushë dhe të kontribojnë bashkarisht drejt zhvillimit të tij.

Diskutimet kryesore gjatë këtyre dy netëve sa zgjati takimi kishin të bënin me softuerin e lirë, openstreetmaps, wikimedia, etj. Pritet që nëndërkohë të punohet shumë më shumë për zhvillimin e Openstreetmaps në Shqipëri. Gjithashtu u diskutua edhe për përkthimet në Wikimedia dhe pati mjaft diskutime në lidhje me përdorimin e gjuhës standarde shqipe në këto përkthime.

Luca Pescatore, nga Italia, ishte gjithashtu pjesëmarrës në takim (dhe natyrisht përkrahës i softuerit të lirë) dhe në ditën e dytë mori fjalën dhe diskutoi për gjërat që kanë të bëjnë me gjenerimin e të hyrave nga softueri i hapur dhe i lirë.

Përkrahësit e softuerit të lirë dhe të hapur diskutuan edhe rreth organizimit të konferencës në Vlorë me 11-12 shtator dhe konferencës tjetër në Prishtinë me 25-26 shtator.

Shiqojeni Galerinë e FLOSSK-ut për ti parë fotot e kësaj ngjarjeje: Kliko Ketu
