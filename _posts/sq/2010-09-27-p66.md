---
layout: post
title: 'Software Freedom Kosova 2010'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/1(1).jpg)
Më 25 dhe 26 shtator Prishtina ishte nikoqire për herë të dytë e Konferencës Kosovare të Lirisë Softuerike. 
	SFK10 per të dytën herë u organizua nga FLOSS Kosova dhe Fakulteti i Inxhinierisë Elektrike dhe Kompjuterike (FIEK) i Universitetit të Prishtinës.
	SFK09 e mbajtur në vitin 2009 u vizitua nga rreth 500 persona të cilët ndoqën rreth 40 ligjërata nga 25 ligjërues. Kësaj here konference ishte më e fokusuar: pati 24 ligjërata nga Kosova, regjioni dhe bota.
	Ligjëruesit kryesor dhe njëherit mysafir nderi të kësaj konference ishin hakerë me renome si:

		Leon Shiman, anëtar bordi i fondacionit që mbikëqyr zhvillimin e sistemit grafik për Linux dhe BSD – x.org, dhe pronar i firmës     konsulente Shiman Associates;

		Rob Savoye, zhvilluesi primar i Gnash ndërsa më parë zhvillues edhe për Debian, Red Hat dhe Yahoo. Savoye programon që nga viti 1977;

		Mikel Maron, anëtar bordi i OpenStreeMap Foundation;

		Peter Salus, linguist, shkencëtar kompjuterik dhe historian i teknologjisë.

	SFK10 është përgatitur në saje të punës pothuajse një vjeçare të komitetit organizativ.
	Tema të tjera ofruan edhe:

		Milot Shala    

		Martin Bekkelund    

		Baki Goxhaj    

		Marco Fioretti

	Konferenca u mbajt në mjediset e FIEK-ut. Konferenca u përkrah financiarisht nga Zyra e Kryeministrit, Rrota, PC World Albanian dhe OpenWorld.al.

	Shiqoje Foto Galerinë e kësaj ngjarjeje: FLOSSK Gallery
	Vizito faqen e konferencës në http://www.kosovasoftwarefreedom.org
