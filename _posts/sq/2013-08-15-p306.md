---
layout: post
title: '(more) Notes from a Debian pilgrim'
lang: sq
category: sq
author: "pete_m"
---
i shall be attending the FLOSSK conference again this year for the third time, and hope to be able to contribute in the form of a Workshop and/or Presentation . .. 

I expect to make this contribution in a very informal way, hoping mostly for  Q&A sessions around my skills and interests in the FLOSS field..

My presentation last year was tagged on at the end of the conference, and so was not well attended. For this reason I shall not be unduly worried to present some of the samematerial again.  .. 

I expect to flesh out the actual content for my contribution in consultation with FLOSSK colleagues to best suit the conference/ workshop context and the interests of attendees.

Here are some notes towards this contribution . ..

(more) Notes from a Debian Pilgrim. .  .

For the past 5 years I have been working with a Linux netbook system, which is Debian-based, and which has successfully followed a 'rolling upgrade' for the last 3 years.

I hope informally to present what I have learnt and to share some insights . .

I migrated from Ubuntu - a process which was far from simple( and attracted some mockery from Debian purists) but which has taught me a great deal about the architecture of Linux systems.

Why Open Source( alone) is not enough - why, and perhaps how to take the FLOSS message to those who need it . .

Placid Linux - the Agnostic Linux distro

Distribution tyranny, politics and why it damages our common cause ..

Agnostic means I don't have the answers, Freedom as in 'freedom of choice'

many distros more or less enforce their choices on users.

This may be good for users who do not want to look 'under the hood' - but surely contributes to ...

Obfuscation and fragmentation

and  the continuing failure of Linux to mount a serious challenge to Windoze in the PC domain.

Beware Ubuntu !! 

Shuttleworth's power grab, 

After X.  .

Inside a distribution . .

Boot Process . . initv, systemd, upstart( no thanks)

Display Managers. X11 - the future

Window Managers, Desktops - gnome, KDE, IceWM Fluxbox, Openbox etc.

Unity, Cinnamon .. 

Applications.  .

The C libraries.  .

Package management, (rolling) upgrades
