---
layout: post
title: 'Software Freedom Kosova 2011 po vjen'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/sfk11-final-small.jpeg)
Konferenca Software Freedom Kosova 2011 është konferenca e tretë vjetore për Softuerin e Lirë dhe me Burim të Hapur (FLOSS) që do të mbahet në Prishtinë më 12 Nëntor.
Eventi është i hapur por ju rekomandojmë që ta rezervoni ulësen tuaj: http://sfk11.eventbrite.com/
Konferenca mbledh profesionistët, akademikët dhe entuziastë të cilët ndajnë vizionin që softueri duhet të jetë i lirë dhe i hapur për komunitetin për ta zhvilluar dhe ta rregullojë për nevojat e tijë, duke përfshirë edhe ato tregtare, dhe se dituria është një pronë komunale e lirë dhe e hapur për të gjithë.
FLOSS Kosova ka arritur të bëjë konferencën një ngjarje tradicionale/vjetore, në të cilën mblidhet një audiencë rajonale vjetore për leksione dhe prezantime nga një shumëllojshmëri profesionistësh ndërkombëtarë dhe vendorë dhe akademikë të cilët do të paraqesin zhvillimet e fundit në komunitetin global të FLOSS.
Dhe vetëm të përmendet se konferenca e parë u mbajt në gushttë vitit 2009 me rreth 500 të pranishmëm dhe folës nga Sun Microsystems, Mozilla, MIT, konsultantë nga Qeveria Open Source e Unionit të Bashkimit Europian dhe të tjerët kurse konferenca e dytë u mbajt në shtator të vitit 2010 dhe kishte prezent disa personazhe të njohura  të botës së FLOSS .
Tema e SFK11
Tema e konferencës 2011 do të jetë "Të bësh biznes me Free Software". Konferenca do të përdoret si një pikë takimi për një rrjet të individëve dhe kompanive me qëllim të sponsorizuar dhe të ndërmarrë projekte nga të cilat përfitojnë qytetarët e Kosovës, duke krijuar atë si një nga qendrat evropiane të FLOSS, dhe në fund të rezultojë në përfitime komerciale për vendit.Konferenca ka për qëllim që ti njoftojë biznesevt kosovare dhe publikun për alternativa me kosto të ulët në vend të softuerëve të shtrenjtë, të trajnojë njerëz lokal në zhvillimet e fundit në softuer dhe mjetet për bashkëpunimin-social, të promovojë respektin për licenca ligjore, dhe të prezantojë publikun me metodat më të mira për lokalizimin e softuerit të njohur.
Konferenca do të paraqesë folës nga bota e biznesit që përdorin FLOSS në projektet e tyre komerciale. Kjo gjithashtu do të paraqesë shembuj të kompanive nga rajoni që janë duke përdorur FLOSS si një këmbë konkurruese në tregun evropian të outsourcing. Softueri i lirë është veçanërisht i përshtatshëm për këtë lloj veprimtarie ekonomike, ngaqë paraqet pengesë shumë të ulët të barierave duke qenë i lirë në blerje, i dokumentuar mirë dhe me një mbështetje të një ekosistemi të lirë. Për më tepër, duke paraqitur kodin e lirë që mund të modifikohet, ajo u ndihmon profesionistëve dhe kompanive për të ndërtuar aftësinë dhe të punojnë drejt udhëheqjes së projekteve me vlera të mëdha.
Për folësit dhe personat nga rajoni që marrin pjesë në këtë konferencë, ju lutem visitoni http://www.flossk.org/en/sfk11-pois për të parë një hartë me Pikat e Interesit (Restorantet, Hotelet, Kafenetë, Bankat etj) në Prishtinë.
	Më poshtë mund të shkarkoni Programin e konferencës.
