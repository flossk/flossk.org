---
layout: post
title: 'Ftesa e Trajnimit për GIS nga Joachim Bergerhoff'
lang: sq
category: sq
author: "admin"
---
Më 2 Tetor 2010, anëtarët e FLOSSK-ut  ishin të ftuar nga Joachim Bergerhoff që të ishin pjesë e prezantimit të OpenSource GIS(Geographic Information System), në këtë prezantim Benjamin Ducke offroi edhe një trajnim të shkurtër për programin FSLU (Further Support to Land Use) në Kosovë.

Subjekti kryesor i këti trajnimi ishte:

- Instalimi i gvSIG
- Integrimi i të dhënave
- Editimi i të dhënave

Për të shkarkuar këtë softuer klikoni këtu:http://oadigital.net/software/gvsigoade/gvsigdownload

Programi është i ndërtuar në gjuhën programuese Java, kështu që funksionon në të gjitha sistemet operative.

Ky program bën editimin e shape fajlleve dhe shumë formateve tjera. Poashtu në të është e perfshirë edhe toolbox-at si GRASS(Geographic Resources Analysis Support System) dhe Sextante dhe shumë mjete tjera...

Këtu mund të shihni prezantimin nga SFK10(Software Freedom Kosova) prej Benjamin Ducke-s: Kliko Ketu