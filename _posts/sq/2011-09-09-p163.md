---
layout: post
title: 'Software Freedom Day së shpejti në Prishtinë'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/sfd.png)
Software Freedom Day (SFD) është një festë vjetore në mbarë botën e Free Software.
	SFD është një përpjekje e arsimit publik me qëllim të ngritjes së vetëdijes së Free Software dhe virtytet e tij, dhe gjithashtu inkurajimin e përdorimit të saj.
	Dhe këtë vit është hera e dytë që organizojmë diçka të tillë.
Në thelb nuk do të ketë asgjë të veçantë, vetëm disa prezantime individuale dhe të kënaqemi.
Kjo ngjarje do të ndodhë në Prishtinë, Kosovë, si kudo tjetër që kjo ngjarje do të organizohet më 17 shtator, këtë rradhë tek ne do të ndodhë më 18 shtator 2011.Takimi do të mbahet në Prince Coffee House në 20:00 (ora lokale Prishtinë)Ju gjithashtu mund të konfirmoni praninë tuaj në Facebook

	Prince Coffee House në hartë:



	Shiqpje hartën më të madhe
