---
layout: post
title: 'Parti i Lansimit të Fedora 15'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/Fedora_kosova.jpg)
Sot, ashtu siç keni pasur mundësi të lajmëroheni në një artikull të mëhershëm nga faqja jonë si dhe PC World Albania, në Prishtinë është mbajtur një takim për të festuar lansimin e versionit të ri të sistemit operativ Fedora.

		Ngjarja është mbajtur në lokalet e UNICEF Innovations Lab.



		Ndeja nuk filloi ashtu siq pritej, prezantimet nuk filluan në kohën e paraparë, në ora 18, për arsye të numrit të vogël të pjesëmarrësve, edhe pse pati premtime nga shumë njerëz për ardhje.

		Me fillimin e prezantimeve u treguan edhe  risitë që përfshihen në versionin e ri të Fedora 15.
		Prezantues në këtë takim ishte Gent Thaçi. Me disa pika kyçe, u bë edhe një përshkrim i shkurtër i gjërave që mund të quhen si më interesante dhe më karakteristike për këtë version.
		Pjesëmarrësit patën rastin që të merrnin disa dhurata simbolike si logo të sistemit Fedora, duke përfshirë edhe CD me Fedora me desktopin GNOME 3, KDE e XFCE
