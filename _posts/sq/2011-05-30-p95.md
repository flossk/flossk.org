---
layout: post
title: 'FLOSSK në NSND-Ohër'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/nsnd_ohrid_thumb.jpg)
Të shtunën më 28 Maj FLOSSK ishte i ftuar në një takim të mbajtur në Ohër, Maqedoni të quajtur "NSND" - Nista Se Nece Dogobiti që do të thotë: "Asgjë nuk do të ndodhë".Ashtu si edhe emri tregon, asgjë nuk ndodhi ...

	Së pari u takuam me pjesëmarrësit e pranishëm në këtë takim, dhe pastaj shkuam në një restorant ku vazhduam të njoftohemi mëtej me njëri-tjetrin, dhe gjithashtu darkuam së bashku.Menjëherë të nesërmen në mëngjes u organizuan disa fjalime, ose më mirë të them prezantime, ku të gjithë pjesëmarrësit treguan disa këshilla, truke dhe ndanë edhe disa nga njohuritë e tyre në punën e që ata bënin.
	Aty pati pak kodim dhe gjithashtu pak hack-ime ...Në këtë ngjarje kishte më shumë se 30 persona të pranishëm, dhe të gjithë ishin me përvojë në kompjuter, por megjithatë do të donim të përmendim veçanërisht kohën e mirë që kaluam së bashku.

	NSND - Asgjë nuk do të ndodhë është takim jo-konferencial që organizohet qdo vit. ( për herë të parë u organizua në Kroaci )Pika kryesore e këtij takimi jo-konferencial është organizimi i një takimi të të gjithë hakerëve dhe kontribuesve të Softuerit të Lirë nga e  gjithë ish-Yougosllava.

	Për më shumë rreth NSND vizitoni faqen zyrtare: Kliko Ketu

	Për të parë galerine e kësaj ngjarjeje: Kliko Ketu
