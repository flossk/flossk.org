---
layout: post
title: 'Doracaku "OSM - krijoni harten tuaj" - Takimet e Promovimit'
lang: sq
category: sq
author: "admin"
---
Pas përfundimit të botimit dhe krjimit të doracakut “Open Street Map - krijoni harten tuaj”, ishte koha që të organizohen takimet per promovimin e këti doracaku.
	Takimi i parë ishte në Prizren, ku me ndihmën e Ajtene Krasniqit dhe Shoqatës së Zejtarëve të Prizrenit u mbajtë takimi në njërën nga sallat e shoqatës.
	Ishte një takim mjaftë mirë i organizuar ku të pranishëm ishin shumë Biznesmen të njohur nga komuna e Prizrenit, bashke me nënkryetari i komunës së Prizrenit.
	Prezantimi filloi nga Taulant Ramabaja i cili tregoi në pika të shkurtëra për FLOSSK, si dhe për aktivitet e organizatës tonë.
	Pastaj vazhdoi prezantimi i librit OpenStreetMap Krijoni hartën tuaj që u bë nga Besfort Guri vet autori i librit.

	Takimi i dytë ishte në Gjakovë si zakonisht Heroid Shehu dhe Jon Berisha ishin ata që ndihmuan në realizimin e takimit në Gjakovë.
	Takim ishte i mirë ku pjesëmarrës ishin një grup të rinjësh të interesuar rreth OpenStreetMap, takimi filloj me prezantimin e përmbajtjes së librit, dhe më pas një demonstrim se si të punohet me aplikacionin Java OpenStreetMap për editimin e OpenStreetMap-it

	Dhe takimi i 3te u mbajtë në zyret e UNICEF Innovations lab ne Prishtine ku ndodhi po i njejti prezantim.

	Në të trija këto takime të pranishmëve iu dha nga nje Doracak si shenjë falënderimi për prezencën e tyre...

	Merrni edhe ju një: “Klikoni Ketu”
	Shiqojeni galerinë për të parë më nga afër ngjarjen në Prizren: Kliko Ketu

	Më shumë rreth doracakut: Kliko Ketu
