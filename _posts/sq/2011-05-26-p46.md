---
layout: post
title: 'Wikipedia dhe Wikimedia'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/wikipedia.png)
Wikimedia dhe Wikipedia janë poashtu një ndër projektet kryesore të FLOSS Kosovës, ku me anë të të cilave mundohemi të promovojmë dhe shpërndajmë edukimin e hapur në Kosovë.
	Anëtarët e FLOSSK-ut janë poashtu editues të Wikipedias ku kontribojnë me shkrimin, dhe përkthimin e materialeve të ndryshme në gjuhën shqipe.

	Varianti në gjuhën shqipe i kësaj enciklopedie të lirë ekziston nën adresën: http://sq.wikipedia.org dhe sot përmban me shume se 31.458 artikuj, dhe ndodhet në vendin e 55-të në rangun Meta
	Mos të harojmë të cekim se ndër edituesit më të mëdhenjë të Wikipedias për materiale shqip, por edhe anglisht por me temë shqiptare është vetë themeluesi i organiztes tonë James Michael Dupont i cili ka deri ne 3000 arikuj të shënuar, ja dhe linku:http://en.wikipedia.org/wiki/User:Mduponthttp://en.wikipedia.org/wiki/Special:Contributions/Mdupont
Cfare është Wikimedia, Wikipedia dhe Enciklopedia ?
Wikimedia është një organizatë ndërkombëtare jo-qeveritare produktet e të cilës mund të përdoren nga të gjithë, e cila i është kushtëzuar kërkimit të dijes së lirë. Për mbledhjen, zhvillimin dhe përhapjen e përmbajtjes shpesh përdoren të ashtëquajturat Wiki. Projekti më i përhapur dhe më i njohur në Wikimedia është Wikipedia - Enciklopedia e Lirë.
Wikipedia është një projekt për të krijuar një enciklopedi të lirë e cila do të shkruhet dhe redaktohet nga lexuesit e saj nga e tërë bota.
	Wikipedia është një encikopedi, shume gjuheshe, e bazuar ne web, në një model të hapur editable. Artikujt e Wikipedia sigurojnë lidhjet për udhëzues të përdoruesit për faqet e lidhura me të dhëna shtesë.
	Wikipedia është shkruar nga shume vullnetarë kryesisht anonim që shkruajnë pa paguar. Çdokush me qasje në Internet mund të shkruajnë dhe të bëjnë ndryshime në artikuj Wikipedia (përveç në raste të caktuara ku redaktimi është i kufizuar, për të parandaluar përçarje apo vandalizem). Përdoruesit mund të kontribuojnë në mënyrë anonime, nën një pseudonim, ose me identitetin e tyre të vërtetë, në qoftë se ata zgjedhin.
	Gjithçka tek Wikipedia është nën licencën GNU Free Documentation License (GFDL), licenca të përputhshme me të, ose materiale të cilat nuk janë nën kushtet e të drejtave të autorit. Redaktorët mbajnë të drejtat e autorit për çfarë shkruajnë, por teksti mbetet i lirë për të tjerë për ta përdorur dhe redaktuar sipas kushteve të GFDL-it ose licencave përkatëse. Për më shumë informacion shikoni Mbi të drejtat e autorit.
Enciklopedia është vepër e madhe shkencore që përfshin njohuri për të gjitha fushat e jetës e të veprimtarisë, të sistemuara në mënyrë të përmbledhur në trajtën e një fjalori.
	Vepra e tillë që përfshin njohuri për një degët të veçant të shkencës, të artit, të teknikës, etj.
	Një enciklopedi (shkruar edhe enciklopedi apo Encyclopaedia) është një përmbledhje gjithëpërfshirëse me shkrim që mban ose informacion nga të gjitha degët e diturisë ose një degë të veçantë të diturisë.
Fondacioni Wikimedia, është një organizate amerikane jo-fitimprurëse me seli San Francisko, Kaliforni, Shtetet e Bashkuara, dhe e organizuar sipas ligjeve të shtetit të Floridës, ku edhe eshte e bazuar fillimisht.
	Ajo operon disa projekte bashkëpunimi online wiki duke përfshirë Wikipedia, Wiktionary, Wikiquote, Wikibooks, Wikisource, Wikimedia Commons, Wikispecies, Wikinews, Wikibooks, Wikimedia Incubator dhe Meta-Wiki.
Për më shumë vizitoni:http://wikipedia.orghttp://en.wikipedia.org/wiki/Main_Page
