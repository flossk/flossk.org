---
layout: post
title: "FLOSSK ka dërguar komente në konsultimin publik për Draft-Strategjinë e turizmit të Prishtinës"
lang: sq
author: "Arianit Dobroshi"
---

FLOSSK ka dërguar komente në konsultimin publik për Draft-Strategjinë e turizmit të Prishtinës.

[Draft-strategjia e komentuar](http://prishtinaonline.com/drejtorite/kultures-rinise-dhe-sportit/draft-strategjia-e-turizmit)

Pjesa relevante per FLOSSK-un dhe wikipedianet:

_Ofrimi i informatave të sakta në platforma të hapura në internet. Një nga mënyrat se si vizitorët marrin informacione gjenerale mbi destinacionin janë platformat e hapura në internet (p.sh. Wikipedia). Informacionet për Prishtinën në këto platforma, megjithatë, janë të mangëta dhe shpesh herë jo të sakta. Sa i përket orientimit, vizitorët kryesisht shërbehen me platformën &quot;Googlemaps&quot;. Por, edhe në këtë rast, ekziston një shpërputhje në mes të emrave të rrugëve që figurojnë në &#39;Googlemaps&#39; dhe atyre të vendosur në pllaka fizike në hyrje të rrugëve. Planifikohet që përmes një ekipi tëadresohet mungesa dhe asimetra e informatave në këto platforma, si dhe të bëhet optimizimi i makinave të kërkimit (&quot;searchoptimizationengine&quot;) në përgjithësi._

Ky eshte fillim i mire, por ne kerkojme nje trajtim me gjitheperfshires qe do ta lehtesonte kontributin e komuniteteve perkatese ne platformat e hapura per ta permiruar permbajtjen per Prishtinen, çofte ne Wikipedia, OpenStreetMap apo platforma te tjera.

E drejta e autorit pengon shperndarjen e materialit turistik prandaj autori qe synon shperndarjen e informacionit pa qellim te kompensimit financiar, sic eshte rasti me Komunen qe shperndane informacion turistik, duhet te heqe dore nga disa pengesa ligjore qe ne menyre automatike te pavetdijshme i krijon Ligji per te drejten e autorit.

Materiali qe krijojne organet publike vec financohet nga publiku prandaj eshte e natyrshme qe t&#39;i kthehet atij pa kompensim shtese; per me teper, eshte e logjikshme qe te lehtsohet shperndarja e materialit qe krijohet per promovim.

Materiali mund te jene fotografite e monumenteve kulturore dhe te natyres (shiko fotografite qe jane mbledhe prej komunitetit te wikipedianeve permes iniciativave [Wiki Loves Monuments](https://commons.wikimedia.org/wiki/Category:Winners_of_Wiki_Loves_Monuments_2016_in_Kosovo) dhe [Wiki Loves Earth](https://commons.wikimedia.org/wiki/Commons:Wiki_Loves_Earth_2016_in_Kosovo/Winners)), vet Enciklopedia  e Lire Wikipedia, harta e lire [OpenStreetMap](https://osm.org/go/xfpE8FFA?layers=T) e cila e mbulon Prishtinen shume me mire se hartat komerciale, guida turistike [Wikivoyage](https://en.wikivoyage.org/wiki/Pristina) e cila eshte shume me aktuale se guidat komerciale etj. Ne momentin qe materiali shkruhet ne anglisht, mund te kerkojme perkthim nga kontribuesit ne keto projekte ne gjuhet tjera, p.sh. ato te Evropes Lindore ose edhe dicka ekzotike si japonishtja.

Keto platforma mund te sherbejne si produkte ne vetvete ose per t&#39;u perdorur per te ndertuar aplikacione dhe materiale derivuese.

P.sh. nje mori fotosh turistike nen licencat e hapura sic jane ato Creative Commons do te lehtsonin shume shkrimin e guidave turistike ose krijimin e aplikacioneve, njejt edhe me OpenStreetMap e cila mund te integrohet pa pagese dhe mund te pasurohet nga kontribuuesit individual.

Ne momentin qe institucioni dhuron material nen licence te hapur dhe teknikisht te riprodhuheshem nga makina ose mbeshtete ne forma te ndryshme keto projekte, e perbashkta (commons) rritet duke krijuar baze per sendertim te metutjeshem te paimagjinueshem nga autori i pare.

Licencat me te njohura jane ato Creative Commons, te cilat mundesojne shperndarjen me disa restriksione varesisht synimit, lexo per to ketu [https://creativecommons.org/licenses/](https://creativecommons.org/licenses/) dhe ketu [https://en.wikipedia.org/wiki/Creative\_Commons\_license](https://en.wikipedia.org/wiki/Creative_Commons_license)

**Komuna e Prishtines duhet te bej keto:**

**- Ta licencoj materialin ekzistues turistik qe ka ne pronesi nen nje licence te hapur.**

**- Te siguroj qe fotot, guidat, hartat dhe softueri qe do te prodhohet ne te ardhmen do te licencohet nen nje licence te hapur dhe ofrohet ne format elektronik burimor (source file) te publikuar online.**

**- Ta nxite prodhimin e permbajtjes nga te tjeret nen licenca te hapura duke mbeshtetur me material burimor nisma si ato te wikipedianve ose hartografuesve ne OpenStreetMap.**
