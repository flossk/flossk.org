---
layout: post
title: 'FLOSSTALK me Betim Devën: Adaptimi i Sistemit Operativ Android'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/android.jpg)
Të Enjten më datën 29.12.2011 në ora 18:00 tek hapsirat e IDI - Information Development Centre do të mbahet një fjalim me temën: Adaptimi i Sistemit Operativ Android për e-readers, telefonat mobil, dhe tabletët.
Ligjërues do të jetë Betim Deva i cili vjen nga Prishtina. Ka baçelor në inxhinieri, sistemet kompjuterike dhe master në shkenca kompjuterike prej Arizona State University.
Zhvillon aplikacione dhe adapton sistemin Android për nook, lexuesin elektronik të Barnes & Noble, kurse para kësaj në mes tjerash ka zhvilluar softuer me kërkesa të vogla të energjisë për Garminfone, paisja e parë me Android e Garmin-it.
Më herët ka zhvilluar softuer shkencor: në mes tjerash ka menaxhuar publikimin e softuerit Davinci, një platformë shkencore me kod të hapur e përdorur kryesisht për të përkrahur THEMIS, instrument në sondën kozmike Mars Odyssey të NASA's.
Programon në Java, C/C++, Perl, teknologjitë e uebit (PHP, HTML, Ajax,Javascript), skriptat shell, Python, C#, Scheme/Lisp, Prolog, Pascal,OpenGL, etj.
Ka bashkthemeluar portalin për shkencë dhe teknologji pr-tech.net dhe faqen për promovimin e pavarësisë së Kosovës KosovoThanksYou.com.
IDI - Information Development Centre, ndodhet në rrugën Gazmend Zajmi (Dubrovniki) Str. No. 34, 10000 Pristina, Kosovo (më saktësisht shiqoni hartën)



	View Larger Map
